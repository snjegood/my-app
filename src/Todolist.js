import React, { Component } from 'react';
import './App.css';
import Todoitem from './Todoitem'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import './Todoitem.css'
import store from './store'
import { getAddItemAction, getChangeInputValueAction, getDelItemAction, getDefaultValue, getInitListWithSaga } from './store/actionCreator'
import { TodolistUI } from './TodolistUI';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this)
    this.handleBtnClicl = this.handleBtnClicl.bind(this)
    this.handleDelItem = this.handleDelItem.bind(this)
    this.handleStoreChange = this.handleStoreChange.bind(this)
    this.state = store.getState()
    store.subscribe(this.handleStoreChange)
  }
  handleStoreChange() {
    this.setState(store.getState)
  }
  handleChange(e) {
    //将this.setState(对象)改为this.setState(函数),便于异步处理

    // this.setState({
    //   inputValue: e.target.value,
    // })
    const value = e.target.value
    // const action = {
    // type: CHANGE_INPUT_VALUE,
    // value: value
    // }
    const action = getChangeInputValueAction(value)
    store.dispatch(action)
    // this.setState(() => ({ inputValue: value }))
    // console.log(value)
  }
  handleBtnClicl() {
    //非空检查，当没有输入任何内容的时候，则提交按钮不改变state内容
    // this.setState((preState) => {
    //   if (preState.inputValue !== '') {
    //     return {
    //       list: [...preState.list, preState.inputValue],
    //       inputValue: ''
    //     }
    //   }
    // })
    // const action = {
    //   type: ADD_ITEM,
    //   value: this.state.inputValue
    // }
    const action = getAddItemAction()
    store.dispatch(action)
  }
  handleDelItem(index) {
    // const newList = [...this.state.list]
    // newList.splice(index, 1)
    // this.setState({
    //   list: newList
    // })
    // this.setState((preState) => {
    //   const newList = [...preState.list]
    //   newList.splice(index, 1)
    //   return {
    //     list: newList
    //   }
    // })
    // const action = {
    //   type: DEL_ITEM,
    //   value: index
    // }
    console.log(index)
    const action = getDelItemAction(index)
    store.dispatch(action)
  }
  getTodoItem() {
    return (<TransitionGroup>
      {this.state.list.map((item, index) => {

        // return <li
        //   key={index}
        //   onClick={this.handleDelItem.bind(this, index)}
        //   dangerouslySetInnerHTML={{ __html: item }}
        // >
        // </li>
        return (
          <CSSTransition
            // show={true}
            timeout={1000}
            classNames='fade'
            // appear组件创建的时候就执行样式库加载
            appear={true}
            key={item}
            content={item}
          >
            <Todoitem
              content={item}
              index={index}
              //下面两种写法都可以，但是不知道区别，写了一下自己的见解，可能不对
              //这种写法是传递函数执行，当子类组件调用delFunc时候，其实是执行{}内的函数
              //其实就是delFunc(index) 就是(index)=>this.handleDelItem(index)
              // delFunc={(delIndex)=>this.handleDelItem(delIndex)}
              //这种写法是传递函数引用，需要指明函数的this指向，当子类调用delFunc时候，是在调用父类的handleDelItem方法
              //其实是做了一个处理 delFunc as handleDelItem 也就是 delFunc(index) 就是handleDelItem(index)
              delFunc={this.handleDelItem}
            >
            </Todoitem>
          </CSSTransition>
        )
      })}
    </TransitionGroup>)

  }
  componentWillMount() {
    // console.log('Todolist will mount')
  }
  componentDidMount() {
    // console.log('Todolist did mount')
    // axios.get('/api/todolist').then((res) => {
    //   this.setState(() => ({
    //     list: res.data
    //   }))
    // })
    // const action = getDefaultValue()
    // store.dispatch(action)
    const action = getInitListWithSaga()
    store.dispatch(action)
  }
  componentWillUpdate() {
    // console.log('Todolist will update')
  }
  componentDidUpdate() {
    // console.log('Todolist did update')
  }
  render() {
    // console.log('Todolist render')
    return (
      // <Fragment>
      //   <Input
      //     name='inputValue'
      //     value={this.state.inputValue}
      //     onChange={this.handleChange}
      //     placeholder='add todo'
      //     style={{ width: 300 }}
      //     onPressEnter={this.handleBtnClicl}
      //   />
      //   <Button type="primary" onClick={this.handleBtnClicl}>提交</Button>
      //   {/* this.getTodoItem()函数调用 this.getTodoItem为函数引用，可以赋值给其他变量(func=this.getTodoItem) */}
      //   {/* {this.getTodoItem()} */}
      //   <List
      //     style={{ width: 300 }}
      //     bordered
      //     dataSource={this.state.list}
      //     renderItem={(item, index) => (
      //       <List.Item onClick={this.handleDelItem.bind(this, index)} key={item}>
      //         {item}
      //         {/* <Todoitem
      //           content={item}
      //           index={index}
      //           //下面两种写法都可以，但是不知道区别，写了一下自己的见解，可能不对
      //           //这种写法是传递函数执行，当子类组件调用delFunc时候，其实是执行{}内的函数
      //           //其实就是delFunc(index) 就是(index)=>this.handleDelItem(index)
      //           // delFunc={(delIndex)=>this.handleDelItem(delIndex)}
      //           //这种写法是传递函数引用，需要指明函数的this指向，当子类调用delFunc时候，是在调用父类的handleDelItem方法
      //           //其实是做了一个处理 delFunc as handleDelItem 也就是 delFunc(index) 就是handleDelItem(index)
      //           delFunc={this.handleDelItem}
      //         >
      //         </Todoitem> */}

      //       </List.Item>
      //     )}
      //   />
      // </Fragment>
      <TodolistUI
        inputValue={this.state.inputValue}
        handleChange={this.handleChange}
        handleBtnClicl={this.handleBtnClicl}
        handleDelItem={this.handleDelItem}
        list={this.state.list}
      ></TodolistUI>
    );
  }
}
export default App;
