import { takeEvery, put } from 'redux-saga/effects'
import { GET_INIT_LIST } from './actionTypes'
import axios from 'axios'
import { initItemList } from './actionCreator'
// import store from '../store'
function* mySaga() {
  //takeEvery捕获action，捕获成功执行后面的函数
  yield takeEvery(GET_INIT_LIST, getInitList)
}
//普通函数
// function getInitList() {
//   console.log('saga')
//   axios.get('/api/todolist').then((res) => {
//     const action = initItemList(res.data)
//     store.dispatch(action)
//   })
// }
//generator函数
function* getInitList() {
  try {
    const res = yield axios.get('/api/todolist')
    const action = initItemList(res.data)
    yield put(action)
  } catch (error) {
    console.log('/api/todolist cannot use')
  }
}
export default mySaga