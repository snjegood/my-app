import { ADD_ITEM, CHANGE_INPUT_VALUE, DEL_ITEM, INIT_LIST } from './actionTypes';

const defaultState = {
  inputValue: '',
  list: []
}
export default (state = defaultState, action) => {
  // console.log(action)
  if (action.type === CHANGE_INPUT_VALUE) {
    const newState = JSON.parse(JSON.stringify(state))
    newState.inputValue = action.value
    return newState
  }
  if (action.type === ADD_ITEM) {
    // console.log(action)
    if (action.value === '') {
      return state
    }
    const newState = JSON.parse(JSON.stringify(state))
    newState.list.push(newState.inputValue)
    newState.inputValue = ''
    return newState
  }
  if (action.type === DEL_ITEM) {
    const newState = JSON.parse(JSON.stringify(state))
    newState.list.splice(action.value, 1)
    return newState
  }
  if (action.type === INIT_LIST) {
    const newState = JSON.parse(JSON.stringify(state))
    newState.list = action.value
    return newState
  }
  return state
}