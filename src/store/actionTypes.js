export const ADD_ITEM = 'add_item'
export const DEL_ITEM = 'del_item'
export const CHANGE_INPUT_VALUE = 'change_input_value'
export const GET_DEFAULT_VALUE = 'get_default_value'
export const INIT_LIST = 'init_list'
export const GET_INIT_LIST = 'get_init_list'