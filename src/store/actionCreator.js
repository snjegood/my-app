import { ADD_ITEM, DEL_ITEM, CHANGE_INPUT_VALUE, INIT_LIST, GET_INIT_LIST } from './actionTypes'
import axios from 'axios'
export const getChangeInputValueAction = (value) => ({
  type: CHANGE_INPUT_VALUE,
  value
})
export const getDelItemAction = (value) => ({
  type: DEL_ITEM,
  value
})
export const getAddItemAction = () => ({
  type: ADD_ITEM
})
export const initItemList = (value) => ({
  type: INIT_LIST,
  value
})
export const getDefaultValue = () => {
  return (dispatch) => {
    axios.get('/api/todolist').then((res) => {
      const action = initItemList(res.data)
      dispatch(action)
    })
  }
}
export const getInitListWithSaga = () => ({
  type: GET_INIT_LIST
})