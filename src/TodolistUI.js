import React, { Fragment } from 'react'
import { Input, Button, List } from 'antd'
import 'antd/dist/antd.css'

export const TodolistUI = (props) => {
  return (
    <Fragment>
      <Input
        name='inputValue'
        value={props.inputValue}
        onChange={props.handleChange}
        placeholder='add todo'
        style={{ width: 300 }}
        onPressEnter={props.handleBtnClicl}
      />
      <Button type="primary" onClick={props.handleBtnClicl}>提交</Button>
      {/* this.getTodoItem()函数调用 this.getTodoItem为函数引用，可以赋值给其他变量(func=this.getTodoItem) */}
      {/* {this.getTodoItem()} */}
      <List
        style={{ width: 300 }}
        bordered
        dataSource={props.list}
        renderItem={(item, index) => (
          <List.Item onClick={() => props.handleDelItem(index)} key={item}>
            {item}
            {/* <Todoitem
                content={item}
                index={index}
                //下面两种写法都可以，但是不知道区别，写了一下自己的见解，可能不对
                //这种写法是传递函数执行，当子类组件调用delFunc时候，其实是执行{}内的函数
                //其实就是delFunc(index) 就是(index)=>this.handleDelItem(index)
                // delFunc={(delIndex)=>this.handleDelItem(delIndex)}
                //这种写法是传递函数引用，需要指明函数的this指向，当子类调用delFunc时候，是在调用父类的handleDelItem方法
                //其实是做了一个处理 delFunc as handleDelItem 也就是 delFunc(index) 就是handleDelItem(index)
                delFunc={this.handleDelItem}
              >
              </Todoitem> */}

          </List.Item>
        )}
      />
    </Fragment>
  );
}