import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Todoitem.css'

class Todoitem extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick() {
    const { delFunc, index } = this.props
    delFunc(index)
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.content !== this.props.content
  }
  componentWillMount() {
    // console.log('Todoitem will mount')
  }
  componentWillUnmount() {
    // console.log('Todoitem will unmount')
  }
  render() {
    // console.log('Todoitem render')
    const { content } = this.props
    return (
      <div
        onClick={this.handleClick}
      >
        {content}
      </div>
    )
  }
}
Todoitem.propTypes = {
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  delFunc: PropTypes.func,
  index: PropTypes.number
}
Todoitem.defaultProps = {
  content: 'sooj'
}
export default Todoitem