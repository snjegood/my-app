import React, { Component, Fragment } from 'react'
import { Input, Button, List } from 'antd'
import 'antd/dist/antd.css'
import { connect } from 'react-redux'
import { getAddItemAction, getChangeInputValueAction, getDelItemAction, getDefaultValue, getInitListWithSaga } from './store/actionCreator'

/**
 * 使用react-redux
 */
class TodoListWithReactRedux extends Component {
  render() {
    return (
      <div>
        <Input
          name='inputValue'
          value={this.props.inputValue}
          onChange={this.props.handleChange}
          placeholder='add todo'
          style={{ width: 300 }}
          onPressEnter={this.props.handleBtnClicl}
        />
        <Button
          type="primary"
          onClick={this.props.handleBtnClicl}
        >提交</Button>
        <List
          style={{ width: 300 }}
          bordered
          dataSource={this.props.list}
          renderItem={(item, index) => (
            <List.Item
              onClick={() => this.props.handleDelItem(index)}
              key={item}>
              {item}
            </List.Item>
          )}
        />
      </div >
    );
  }
  componentDidMount() {
    // this.props.initList
  }
}
const mapStateToProps = (state) => {
  return {
    inputValue: state.inputValue,
    list: state.list
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    handleChange(e) {
      const action = getChangeInputValueAction(e.target.value)
      dispatch(action)
    },
    handleBtnClicl() {
      const action = getAddItemAction()
      dispatch(action)
    },
    handleDelItem(index) {
      const action = getDelItemAction(index)
      dispatch(action)
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoListWithReactRedux)