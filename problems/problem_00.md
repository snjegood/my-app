### 关于父组件方法传递给子组件的问题
delFunc={(delIndex)=>this.handleDelItem(delIndex)
delFunc={this.handleDelItem.bind(this)
上述两种方式都可以将handleDelItem函数传递给子组件，子组件调用delFunc(index)就可以执行父组件的handleDelItem方法